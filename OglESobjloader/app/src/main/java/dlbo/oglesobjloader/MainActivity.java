package dlbo.oglesobjloader;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import min3d.core.Object3dContainer;
import min3d.parser.IParser;
import min3d.parser.Parser;
import min3d.vos.Light;

/*

    Requirement :
        주어진 얼굴 obj model 및 texture 데이터로 3D 얼굴 렌더링해서 안드로이드 터치 이벤트에 따라 얼굴도 같이 돌아가는 과제이며,
        Android  및 OpenGL을 이용하여 안드로이드 폰상에서 구동되는 앱을 구현하여야 합니다.
        (검은 화면에 얼굴만 나오면 됩니다. 일체의 GUI 필요 없음)

    Description :
        1. 시어스랩 면접 과제용 코드
*/

public class MainActivity extends min3d.core.RendererActivity {

    /* 선언부 */
    private Object3dContainer objFile;         // 3d OBJ 파일 모델
    private float fPrevX, fPrevY;            // 이전 터치 좌표, 회전 계산용
    private float fYaw, fPitch;              // 회전 연산용
    private final float fUnitSpeed = 10.0f; // 회전 속도 조정용

    @Override
    public void initScene()
    {
        // obj파일 로드
        IParser parser = Parser.createParser(Parser.Type.OBJ,
                getResources(), "dlbo.oglesobjloader:raw/head_obj", true);
        parser.parse();

        // Object 로드
        objFile = parser.getParsedObject();
        // 노트5 기준 너무 크기가 작기 때문에 5배 스케일링 수행
        objFile.scale().x = objFile.scale().y = objFile.scale().z = 5.0f;
        scene.addChild(objFile);

        // 광원 추가,
        // 너무 어둡게 나오므로 광원 6개로 전방향을 모두 조명
        Light light = new Light();
        light.position.setX(-5.0f);
        light.position.setY(0.0f);
        light.position.setZ(0.0f);
        scene.lights().add(light);

        light = new Light();
        light.position.setX(5.0f);
        light.position.setY(0.0f);
        light.position.setZ(0.0f);
        scene.lights().add(light);

        light = new Light();
        light.position.setX(0.0f);
        light.position.setY(5.0f);
        light.position.setZ(0.0f);
        scene.lights().add(light);

        light = new Light();
        light.position.setX(0.0f);
        light.position.setY(-5.0f);
        light.position.setZ(0.0f);
        scene.lights().add(light);

        light = new Light();
        light.position.setX(0.0f);
        light.position.setY(0.0f);
        light.position.setZ(5.0f);
        scene.lights().add(light);

        light = new Light();
        light.position.setX(0.0f);
        light.position.setY(0.0f);
        light.position.setZ(-5.0f);
        scene.lights().add(light);

        // 터치 이벤트 발생 시 회전변환에 대응하는 구현
        _glSurfaceView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                // 터치가 시작되는 순간 좌표 획득
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    fPrevX = event.getX();
                    fPrevY = event.getY();
                }
                // 터치를 놓으면 회전하지 않도록 고정
                else if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    fYaw = 0;
                    fPitch = 0;
                }
                // 터치 상태에서 움직이고 있을 때만 회전하도록 설정
                else if (event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    // 적당한 속도로 움직이게 하기 위해서 좌표간 차이를 10으로 나눠서 회전각속도 조정
                    fYaw = ((fPrevX - event.getX()) / fUnitSpeed);
                    fPitch = ((fPrevY - event.getY()) / fUnitSpeed);

                    fPrevX = event.getX();
                    fPrevY = event.getY();
                }

                return true;
            }
        });
    }

    // 회전 수행
    @Override
    public void updateScene() {
        objFile.rotation().y += fYaw;
        objFile.rotation().x -= fPitch;
    }
}
